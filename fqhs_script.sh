#!/bin/sh
# by lanse
route_vlan=`/sbin/ifconfig br0 |grep "inet addr"| cut -f 2 -d ":"|cut -f 1 -d " " `
username=`nvram get http_username`
Fqhs_up=/etc/storage/dnsmasq.d/fqhs_update.sh
Dnsq=/etc/storage/dnsmasq/dnsmasq.conf
FHQ=/etc/storage/post_iptables_script.sh
echo
clear
echo "-----------------------------------------------------------------------------------------------------------------------------------------------------------"
echo -e "\e[1;31m--全自动安装脚本--\e[1;36m翻网看世界\e[1;31m--\e[1;36m(padavan)专用\e[1;31m--\e[1;36m自动运行脚本\e[1;31m包含去AD规则\e[0m"
echo "-----------------------------------------------------------------------------------------------------------------------------------------------------------"
echo
echo -e "\e[1;36m >         1. 安装 \e[1;35m padavan \e[1;36m 翻墙去广告\e[0m"
echo
echo -e "\e[1;33m >         2. 更新 \e[0m"
echo
echo -e "\e[1;36m >         3. 退出 \e[0m"
echo
echo -e "\e[1;31m >         4. 卸载 \e[0m"
echo
echo -e -n "\e[40;37m 请输入数字继续执行: \e[0m" 
read menu
if [ "$menu" == "1" ]; then
echo
echo -e "\e[1;36m 三秒后开始安装......\e[0m"
echo
sleep 3
echo -e "\e[1;36m 创建 dnsmasq 规则与更新脚本存放的文件夹 \e[0m"
if [ ! -d  "/etc/storage/dnsmasq.d" ]; then
	mkdir -p -m 755 /etc/storage/dnsmasq.d
fi
echo
sleep 2
echo -e "\e[1;36m 创建自定义扶墙规则 \e[0m"
echo
echo "# 删除 address 前 # 生效，如有需要自己添加的规则，请在下面添加
# Custom user servers file for dnsmasq
# 特定域名的自定义DNS设置例子:
#server=/mit.ru/izmuroma.ru/10.25.11.30
#address=/.001union.com/127.0.0.1
#国内dns优化
server=/.qq./119.29.29.29
server=/.taobao./223.6.6.6
server=/.alicdn./223.6.6.6" > /etc/storage/dnsmasq.d/userlist
echo
sleep 2
echo -e "\e[1;36m 创建自定义广告黑名单 \e[0m"
echo
echo "# 请在下面添加广告黑名单
# 每行输入要屏蔽广告网址不含http://符号
hm.baidu.com
active.admore.com.cn
mtty-cdn.mtty.com
static-alias-1.360buyimg.com
image.yzmg.com" > /etc/storage/dnsmasq.d/blacklist
echo
sleep 2
echo -e "\e[1;36m 创建自定义广告白名单 \e[0m"
echo
echo "# 请将误杀的网址添加到在下面白名单
# 每行输入相应的网址或关键词即可，建议尽量输入准确的网址
m.iqiyi.com
m.v.qq.com
toutiao.com
dl.360safe.com
down.360safe.com
fd.shouji.360.cn
zhushou.360.cn
shouji.360.cn
hot.m.shouji.360tpcdn.com
jd.com
tejia.taobao.com
temai.taobao.com
ai.m.taobao.com
ai.taobao.com
re.taobao.com
shi.taobao.com
tv.sohu.com
s.click.taobao.com
s.click.tmall.com
ju.taobao.com
t10.baidu.com
t11.baidu.com
t12.baidu.com" > /etc/storage/dnsmasq.d/whitelist
echo
sleep 2
echo -e "\e[1;31m 准备翻网去广告规则文件 \e[0m"
echo
echo -e "\e[1;36m 下载 'easylistchina' FQ 规则 \e[0m"
echo
wget --no-check-certificate -t 10 -T 30 http://c.nnjsx.cn/GL/dnsmasq/update/adblock/easylistchina.txt -cO /tmp/fq1.conf
echo
# 下载 'sy618''vokins' 组合 FQ 规则
echo
wget --no-check-certificate https://raw.githubusercontent.com/896660689/Hsfq/master/TMP_FQ -qO \
/tmp/fq.conf && sleep 2 && chmod +x /tmp/fq.conf && . /tmp/fq.conf
echo
echo -e "\e[1;36m 下载 'yhosts''malwaredomainlist''sy619hosts' HS 规则 \e[0m"
echo
wget --no-check-certificate -t 10 -T 30 http://c.nnjsx.cn/GL/dnsmasq/update/adblock/malwaredomainlist.txt -O /tmp/hs1.conf
echo
wget --no-check-certificate -t 10 -T 30 http://c.nnjsx.cn/GL/dnsmasq/update/adblock/yhosts.txt -O /tmp/hs2.conf
echo
wget --no-check-certificate -t 20 -T 60 http://raw.githubusercontent.com/vokins/yhosts/master/hosts -O /tmp/hs3.conf
# 下载 'winhelp200''someonewhocares''malwaredomainlist''hosts-file' HOSTS 规则
echo
echo -e "\e[47;34m 下载时间可能稍长.请耐心等待....\e[0m"
echo
wget --no-check-certificate https://raw.githubusercontent.com/896660689/Hsfq/master/TMP_AD -qO \
/tmp/ad.conf && sleep 2 && chmod +x /tmp/ad.conf && . /tmp/ad.conf
echo
# 下载 '网络收集' HOSTS 规则
wget --no-check-certificate https://raw.githubusercontent.com/896660689/Hsfq/master/TMP_ABD -qO \
/tmp/abd.conf && sleep 2 && chmod +x /tmp/abd.conf && . /tmp/abd.conf
sleep 2
echo
echo -e "\e[1;31m 整合排序文件 \e[0m"
echo
echo -e "\e[1;36m 创建用户自定规则缓存 \e[0m"
cp /etc/storage/dnsmasq.d/userlist /tmp/userlist;sed -i "1 i\## update：$(date '+%Y-%m-%d %H:%M:%S')" /etc/storage/dnsmasq.d/userlist
echo
echo -e "\e[1;36m 创建自定义广告黑名单缓存 \e[0m"
cp /etc/storage/dnsmasq.d/blacklist /tmp/blacklist
sed -i "/#/d" /tmp/blacklist
sed -i 's/^/127.0.0.1 &/g' /tmp/blacklist
echo
echo -e "\e[1;36m 合并 'FQ' 缓存 \e[0m"
cat /tmp/userlist /tmp/fq /tmp/fq1.conf > /tmp/hosts_fq
#cat /tmp/blacklist /tmp/hs1.conf /tmp/hs2.conf /tmp/hs3.conf /tmp/ad > /tmp/hosts_ad
cat /tmp/blacklist /tmp/hs1.conf /tmp/hs2.conf /tmp/hs3.conf /tmp/ad /tmp/abd > /tmp/hosts_ad
echo
sleep 2
echo -e "\e[1;36m 删除 'FQ''HS' 临时文件 \e[0m"
rm -rf /tmp/userlist
rm -rf /tmp/blacklist
rm -rf /tmp/fq1.conf
rm -rf /tmp/fq
rm -rf /tmp/fq.conf
rm -rf /tmp/hs1.conf
rm -rf /tmp/hs2.conf
rm -rf /tmp/hs3.conf
rm -rf /tmp/ad.conf
rm -rf /tmp/ad
rm -rf /tmp/abd.conf
rm -rf /tmp/abd
echo
echo -e "\e[1;36m 删除被误杀的广告规则 \e[0m"
if [ -s /etc/storage/dnsmasq.d/whitelist ]; then
	cat /etc/storage/dnsmasq.d/whitelist | while read line;do
	sed -i "/$line/d" /tmp/hosts_fq
	sed -i "/$line/d" /tmp/hosts_ad
done
fi
echo
echo -e "\e[1;36m 删除注释和本地规则 \e[0m"
sed -i '/::1/d' /tmp/hosts_fq
sed -i '/localhost/d' /tmp/hosts_fq
sed -i '/#/d' /tmp/hosts_fq
sed -i '/#address/d' /tmp/hosts_fq
sed -i '/#/d' /tmp/hosts_ad
sed -i '/@/d' /tmp/hosts_ad
sed -i '/::1/d' /tmp/hosts_ad
sed -i '/localhost/d' /tmp/hosts_ad
echo
echo -e "\e[1;36m 'HOST' 规则格式排序 \e[0m"
sed -i "s/0.0.0.0/127.0.0.1/g" /tmp/hosts_fq
sed -i "s/  / /g" /tmp/hosts_fq
sed -i "s/  / /g" /tmp/hosts_ad
sed -i "s/	/ /g" /tmp/hosts_ad
sed -i "s/0.0.0.0/127.0.0.1/g" /tmp/hosts_ad
echo
echo -e "\e[1;36m 创建 'dnsmasq' 规则文件 \e[0m"
if [ ! -d  "/etc/storage/dnsmasq.d/conf" ]; then
	mkdir -p /etc/storage/dnsmasq.d/conf
fi
echo "
############################################################
##【by (c) 2014-2017, lanse】                              ##
############################################################
# Localhost (DO NOT REMOVE) Start
address=/$route_vlan/127.0.0.1
address=/$route_vlan/::1
address=/ip6-$route_vlan/::1
#address=/ip6-$route_vlan/::1
# Localhost (DO NOT REMOVE) End
# Modified DNS start
" > /etc/storage/dnsmasq.d/conf/hosts_fq.conf;chmod 644 /etc/storage/dnsmasq.d/conf/hosts_fq.conf
echo
echo -e "\e[1;36m 创建 'hosts' 规则文件 \e[0m"
if [ ! -d  "/etc/storage/dnsmasq.d/hosts" ]; then
	mkdir -p /etc/storage/dnsmasq.d/hosts
fi
echo "# Custom user hosts file
# Example: hosts 例子:
# 192.168.2.80		Boo
############################################################
##【by (c) 2014-2017, lanse】                              ##
############################################################
#默认hosts开始（想恢复最初状态的hosts，只保留前三行即可）
127.0.0.1 $route_vlan
::1	$route_vlan
::1	ip6-$route_vlan
::1	ip6-$route_vlan

#默认hosts结束" > /etc/storage/dnsmasq.d/hosts/hosts_ad.conf;chmod 644 /etc/storage/dnsmasq.d/hosts/hosts_ad.conf
echo
echo -e "\e[1;36m 删除 'HOST' 重复规则及相关临时文件 \e[0m"
sort -n /tmp/hosts_fq |uniq >> /etc/storage/dnsmasq.d/conf/hosts_fq.conf
sort -n /tmp/hosts_ad |uniq >> /etc/storage/dnsmasq.d/hosts/hosts_ad.conf
echo
echo "# Modified DNS end" >> /etc/storage/dnsmasq.d/conf/hosts_fq.conf
echo "# 修饰hosts结束" >> /etc/storage/dnsmasq.d/hosts/hosts_ad.conf
echo
sleep 3
echo -e "\e[1;36m 删除合并临时文件 \e[0m"
echo
rm -rf /tmp/hosts_fq
rm -rf /tmp/hosts_ad
echo -e "\e[1;33m 获取规则更新脚本 \e[0m"
[ -f $Fqhs_up ] && rm -rf $Fqhs_up
echo
wget --no-check-certificate -c -O $Fqhs_up http://raw.githubusercontent.com/896660689/Hsfq/master/fqhs_update.sh
sleep 3
echo
echo -e "\e[1;31m 添加计划任务 \e[0m"
sed -i '/fqhs_update.sh/d' /etc/storage/cron/crontabs/$username
sed -i '$a 30 5 * * * /bin/sh /etc/storage/dnsmasq.d/fqhs_update.sh' /etc/storage/cron/crontabs/$username
chmod 755 $Fqhs_up
echo
killall crond;/usr/sbin/crond
sleep 3 
echo -e "\e[1;36m 添加 'dnsmasq.conf' 启动路径 \e[0m"
if [ -f $Dnsq ]; then	
	sed -i '/1800/d' $Dnsq
	sed -i '/storage/d' $Dnsq
	sed -i '$a min-cache-ttl=1800' $Dnsq
	sed -i '$a conf-dir=/etc/storage/dnsmasq.d/conf' $Dnsq
	sed -i '$a addn-hosts=/etc/storage/dnsmasq.d/hosts' $Dnsq
	sed -i '$a ### servers-file=/etc/storage/dnsmasq.d/userlist' $Dnsq
	sed -i '$a ### conf-file=/etc/storage/dnsmasq.d/conf/hosts_fq.conf' $Dnsq
	
fi
echo
sleep 3
echo -e "\e[1;36m 创建上游DNS配置文件 \e[0m"
ROLV=/etc/storage/dnsmasq.d/resolv.conf
echo "## DNS解析服务器设置
nameserver 127.0.0.1
## 以下根据网络环境选择最快的服务器.最多6个地址按速排序

nameserver 223.6.6.6
nameserver 8.8.4.4
nameserver 114.114.114.119
nameserver 176.103.130.131
nameserver 119.29.29.29
nameserver 4.2.2.2" >> $ROLV;chmod 644 $ROLV
sleep 2
cat $ROLV > /etc/resolv.conf;chmod 644 /etc/resolv.conf
echo
sleep 2
echo -e "\e[1;36m 添加防火墙 53 端口转发规则 \e[0m"
if [ -f $FHQ ]; then
	sed -i '/DNAT/d' $FHQ
	sed -i '$a /bin/iptables -t nat -A PREROUTING -p udp --dport 53 -j DNAT --to $route_vlan' $FHQ
	sed -i '$a /bin/iptables -t nat -A PREROUTING -p tcp --dport 53 -j DNAT --to $route_vlan' $FHQ
	sed -i '$a /bin/iptables-save' $FHQ
fi
echo
if [ -f $FHQ ]; then
	sed -i '/resolv/d' $FHQ
	sed -i '/restart_dhcpd/d' $FHQ
	sed -i '$a cp -f /etc/storage/dnsmasq.d/resolv.conf /etc/resolv.conf' $FHQ
	sed -i '$a restart_dhcpd' $FHQ
fi
sleep 2
echo -e "\e[1;31m 重启 dnsmasq 服务 \e[0m"
rm -rf /tmp/fqhs_an.sh
restart_dhcpd && /usr/sbin/dnsmasq restart >/dev/null 2>&1
echo
echo -e "\e[1;36m 定时计划任务添加完成！\e[0m"
sleep 2
echo
clear
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "+                                                          +"
echo "+                 installation is complete                 +"
echo "+                                                          +"
echo "+                     Time:`date +'%Y-%m-%d'`                      +"
echo "+                                                          +"
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo
sh /tmp/fqhs_script.sh
fi
echo
if [ "$menu" == "2" ]; then
	sh $Fqhs_up
	echo
	sleep 2
	echo -e "\e[1;34m 更新已完成,返回主菜单 \e[0m"
	echo
	sleep 3
	sh /tmp/fqhs_script.sh
	echo
fi
echo
if [ "$menu" == "3" ]; then
	rm -f /tmp/fqhs_an.sh
	rm -f /tmp/fqhs_script.sh
	echo
	exit 0
fi
echo
if [ "$menu" == "4" ]; then
	echo
	echo -e "\e[1;31m 开始卸载 dnsmasq 扶墙及广告规则 \e[0m"
	sed -i '/1800/d' $Dnsq
	sed -i '/storage/d' $Dnsq
	sleep 2
	echo
	echo -e "\e[1;31m 删除残留文件夹以及配置 \e[0m"
	rm -rf /etc/storage/dnsmasq.d
	echo
	sleep 2
	echo -e "\e[1;31m 删除相关计划任务 \e[0m"
	sed -i '/fqhs/d' /etc/storage/cron/crontabs/$username
	killall crond;/usr/sbin/crond
	echo
	sleep 2
	echo -e "\e[1;31m 删除防火墙规则 \e[0m"
	sed -i '/DNAT/d' $FHQ
	sed -i '/iptables-save/d' $FHQ
	sed -i '/resolv/d' $FHQ
	sed -i '/restart_dhcpd/d' $FHQ
	echo
	sleep 2
	echo -e "\e[1;31m 重启 dnsmasq \e[0m"
	rm -f /tmp/fqhs_an.sh
	restart_dhcpd && /usr/sbin/dnsmasq restart >/dev/null 2>&1
	echo
	sleep 2
	echo -e "\e[1;34m 卸载完毕,返回主菜单 \e[0m"
	echo
	sleep 3
	sh /tmp/fqhs_script.sh
	echo
fi
echo
