#!/bin/sh
# by lanse
LOGTIME=$(date "+%m-%d %H:%M:%S")
route_vlan=`/sbin/ifconfig br0 |grep "inet addr"| cut -f 2 -d ":"|cut -f 1 -d " " `
username=`nvram get http_username`
fq_lock=/var/lock/fqhs_update.lock
[ -f $fq_lock ] && rm $fq_lock
touch $fq_lock && logger -t "【$LOGTIME】" " HOSTS 翻网去广告规则开始更新..."
echo " Copyright (c) 2014-2017,by lanse"
echo -e "\e[1;36m 3 秒钟后开始更新规则 \e[0m"
echo
sleep 3
echo " 准备 'FQ' 规则文件 "
# 下载 'easylistchina' FQ 规则
wget --no-check-certificate -t 10 -T 30 http://c.nnjsx.cn/GL/dnsmasq/update/adblock/easylistchina.txt -cO /tmp/fq1.conf
echo
# 下载 'sy618''vokins' 组合 FQ 规则
echo
wget --no-check-certificate https://raw.githubusercontent.com/896660689/Hsfq/master/TMP_FQ -qO \
/tmp/fq.conf && sleep 2 && chmod +x /tmp/fq.conf && . /tmp/fq.conf
echo
# 添加用户自定规则缓存
cp /etc/storage/dnsmasq.d/userlist /tmp/userlist

# 合并 dnsmasq 缓存
cat /tmp/userlist /tmp/fq1.conf /tmp/fq > /tmp/hosts_fq

# 删除 dnsmasq 缓存
rm -rf /tmp/userlist
rm -rf /tmp/fq1.conf
rm -rf /tmp/fq
rm -rf /tmp/fq.conf

# 删除误杀广告规则
if [ -s /etc/storage/dnsmasq.d/whitelist ]; then
	cat /etc/storage/dnsmasq.d/whitelist | while read line;do
	sed -i "/$line/d" /tmp/hosts_fq
done
fi

# 删除本地规则
sed -i '/::1/d' /tmp/hosts_fq
sed -i '/localhost/d' /tmp/hosts_fq
sed -i '/#/d' /tmp/hosts_fq
sed -i '/#address/d' /tmp/hosts_fq

echo -e -n " 翻网 DNS 规则排序 "
sed -i "s/0.0.0.0/127.0.0.1/g" /tmp/hosts_fq
sed -i "s/  / /g" /tmp/hosts_fq

# 创建 dnsmasq 规则文件
cat > "/tmp/hosts_fq.conf" <<EEE
############################################################
##【by (c) 2014-2017, lanse】                              ##
############################################################
# Localhost (DO NOT REMOVE) Start
address=/$route_vlan/127.0.0.1
address=/$route_vlan/::1
address=/ip6-$route_vlan/::1
#address=/ip6-$route_vlan/::1
# Localhost (DO NOT REMOVE) End
# Modified DNS start
EEE

# 删除 dnsmasq 重复规则
sort -n /tmp/hosts_fq |uniq >> /tmp/hosts_fq.conf

# 修饰结束
sed -i '$a # Modified DNS end' /tmp/hosts_fq.conf

echo " 更新 hosts_fq 规则."
cp -f /tmp/hosts_fq.conf /etc/storage/dnsmasq.d/conf/hosts_fq.conf

# 删除 dnsmasq 合并缓存
echo
rm -rf /tmp/hosts_fq
rm -rf /tmp/hosts_fq.conf

echo " 准备'HOSTS'规则文件 "
# 下载 'malwaredomainlist' HS 规则
wget --no-check-certificate -t 10 -T 30 http://c.nnjsx.cn/GL/dnsmasq/update/adblock/malwaredomainlist.txt -cO /tmp/hs1.conf
# 下载 'sy619hosts' AD 规则
wget --no-check-certificate -t 10 -T 30 http://c.nnjsx.cn/GL/dnsmasq/update/adblock/yhosts.txt -cO /tmp/hs2.conf
echo
wget --no-check-certificate -t 20 -T 60 https://raw.githubusercontent.com/vokins/yhosts/master/hosts -cO /tmp/hs3.conf
echo
# 下载 'winhelp200''someonewhocares''malwaredomainlist''hosts-file' HOSTS 规则
echo
echo -e "\e[47;34m 下载时间可能稍长.请耐心等待....\e[0m"
echo
wget --no-check-certificate https://raw.githubusercontent.com/896660689/Hsfq/master/TMP_AD -qO \
/tmp/ad.conf && sleep 2 && chmod +x /tmp/ad.conf && . /tmp/ad.conf
echo
# 下载 '网络收集' HOSTS 规则
wget --no-check-certificate https://raw.githubusercontent.com/896660689/Hsfq/master/TMP_ABD -qO \
/tmp/abd.conf && sleep 2 && chmod +x /tmp/abd.conf && . /tmp/abd.conf
echo
echo -e " 导入自定义广告黑名单缓存规则 "
cp /etc/storage/dnsmasq.d/blacklist /tmp/blacklist
sed -i "/#/d" /tmp/blacklist
sed -i 's/^/127.0.0.1 &/g' /tmp/blacklist
echo
# 合并 hosts 缓存
#cat /tmp/blacklist /tmp/hs1.conf /tmp/hs2.conf /tmp/hs3.conf /tmp/ad > /tmp/hosts_ad
cat /tmp/blacklist /tmp/hs1.conf /tmp/hs2.conf /tmp/hs3.conf /tmp/ad /tmp/abd > /tmp/hosts_ad

# 删除 hosts 缓存
rm -rf /tmp/blacklist
rm -rf /tmp/hs1.conf
rm -rf /tmp/hs2.conf
rm -rf /tmp/hs3.conf
rm -rf /tmp/ad
rm -rf /tmp/ad.con
rm -rf /tmp/abd
rm -rf /tmp/abd.con

# 删除误杀广告规则
if [ -s /etc/storage/dnsmasq.d/whitelist ]; then
	cat /etc/storage/dnsmasq.d/whitelist | while read line;do
	sed -i "/$line/d" /tmp/hosts_ad
done
fi

# 删除注释和本地规则
sed -i '/#/d' /tmp/hosts_ad
sed -i '/@/d' /tmp/hosts_ad
sed -i '/localhost/d' /tmp/hosts_ad
sed -i '/::1/d' /tmp/hosts_ad

echo -e -n " 去广告规则排序 "
sed -i "s/  / /g" /tmp/hosts_ad
sed -i "s/	/ /g" /tmp/hosts_ad
sed -i "s/0.0.0.0/127.0.0.1/g" /tmp/hosts_ad

# 创建 hosts 规则文件
echo "# Custom user hosts file
# Example: hosts 例子:
# 192.168.2.80		Boo
############################################################
##【by (c) 2014-2017, lanse】                              ##
############################################################
#默认hosts开始（想恢复最初状态的hosts，只保留前三行即可）
127.0.0.1 $route_vlan
::1	$route_vlan
::1	ip6-$route_vlan
::1	ip6-$route_vlan
#默认hosts结束" > /tmp/hosts_ad.conf

# 删除 hosts 重复规则
sort -n /tmp/hosts_ad |uniq >> /tmp/hosts_ad.conf


# 修饰结束
sed -i '$a # 修饰 hosts 结束' /tmp/hosts_ad.conf

# 删除 hosts 合并缓存
rm -rf /tmp/hosts_ad

echo " 更新 hosts_ad 规则."
cp -f /tmp/hosts_ad.conf /etc/storage/dnsmasq.d/hosts/hosts_ad.conf

sleep 2
# 删除临时文件
rm -f /tmp/hosts_ad.conf
rm -f /tmp/fqhs_update.sh

echo " 重启 dnsmasq 服务."
restart_dhcpd && /usr/sbin/dnsmasq restart >/dev/null 2>&1
echo
echo -e "\e[1;33m 规则更新成功 \e[0m"
sleep 3
rm $fq_lock
exit 0
